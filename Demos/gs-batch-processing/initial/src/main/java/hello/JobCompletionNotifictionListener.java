package hello;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Administrator on 2017/4/5.
 */
@Component
public class JobCompletionNotificationListener extends JobExecutionListener {
    private static final Logger log= LoggerFactory.getLogger(JobCompletionNotificationListener.class);
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public JobCompletionNotificationListener(JdbcTemplate jdbctemplate){
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void afterJob(JobExecution jobExecution){
        if(JobExecution.getStatus()== BatchStatus.COMPLETED){
            log.info("!!! JOB FINISHED! Time to verify the results");
            List<Person> results=jdbcTemplate.query("SELECT first_name,last_name FROM people",);
        }
    }
}
