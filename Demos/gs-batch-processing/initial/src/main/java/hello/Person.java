package hello;

/**
 * Created by Administrator on 2017/4/5.
 */
public class Person {
    public Person(){}
    public Person(String lastName,String firstName){
        this.firstName = firstName;
        this.lastName = lastName;
    }

    private String lastName;
    public String getLastName(){ return this.lastName; }
    public void setLastName(String lastName){ this.lastName = lastName; }

    private String firstName;
    public String getFirstName(){ return this.firstName;}
    public void setFirstName(String firstName){this.firstName=firstName;}

    @Override
    public String toString() {
        return "firstName:" + firstName + ",lastName:" + lastName;
    }
}
