package hello;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

/**
 * Created by Administrator on 2017/4/1.
 */
@Component
public class Receiver {
    @JmsListener(destination="mailbox",containerFactory="myFactory")
    public void receiveMessage(Email email){
          System.out.println("Received<"+email+">");
    }
}
