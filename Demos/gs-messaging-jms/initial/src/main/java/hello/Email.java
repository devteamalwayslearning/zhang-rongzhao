package hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/4/1.
 */

public class Email implements Serializable {
    public Email(){ }
    public Email(String to,String body){
        this.to=to;
        this.body=body;
    }

    private String to;
    public String getTo(){ return this.to; }
    public void setTo(String to){ this.to=to; }


    private String body;
    public String getBody(){ return this.body; };
    public void setBody(String body){ this.body = body; }

    @Override
    public String toString(){
        return "Email{to="+this.to+",body="+this.body+"}";
    }
}
