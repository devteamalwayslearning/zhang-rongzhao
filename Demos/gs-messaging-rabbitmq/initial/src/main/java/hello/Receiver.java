package hello;

import org.springframework.stereotype.Component;
import java.util.concurrent.CountDownLatch;

/**
 * Created by Administrator on 2017/3/29.
 */

@Component
public class Receiver {
    public CountDownLatch latch=new CountDownLatch(1);

    public void receiveMessage(String message){
        System.out.println("Received<"+message+">");
        latch.countDown();
    }

    public CountDownLatch getLatch(){
        return latch;
    }
}
