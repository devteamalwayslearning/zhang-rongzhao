package hello;

/**
 * Created by Administrator on 2017/3/24.
 */
public class Customer {
    public Customer(long id,String firstName,String lastName){
        this.id=id;
        this.firstName=firstName;
        this.lastName=lastName;
    }

    private long id;
    public void setId(long id){this.id=id;}
    public long getId(){return this.id;}

    private String firstName;
    public void setFirstName(String firstName){this.firstName=firstName;}
    public String getFirstName(){return this.firstName;}

    private String lastName;
    public void setLastName(String lastName){this.lastName=lastName;}
    public String getLastName(){return this.lastName;}

    @Override
    public String toString(){
        return String.format("Customer[id=%d,firstName='%s',lastName='%s']",id,firstName,lastName);
    }
}
