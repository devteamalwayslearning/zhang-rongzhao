package hello;

import java.util.List;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.CrudRepository;
/**
 * Created by Administrator on 2017/3/29.
 */

public interface PersonRepository extends GraphRepository<Person> {
    Person findByName(String name);
}
