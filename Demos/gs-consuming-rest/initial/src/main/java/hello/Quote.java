package hello;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * Created by Administrator on 2017/3/23.
 */

@JsonIgnoreProperties(ignoreUnknown=true)
public class Quote {
    public Quote(){}

    private String type;
    public void setType(String type){this.type=type;}
    public String getType(){return this.type;}

    private Value value;
    public void setValue(Value value){this.value=value;}
    public Value getValue(){return this.value;}

    @Override
    public String toString(){
        return "Quote{"+"type='"+type+"\'"+",value="+value+"}";
    }
}
