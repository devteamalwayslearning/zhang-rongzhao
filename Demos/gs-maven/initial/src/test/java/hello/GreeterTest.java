package hello;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Created by Administrator on 2017/3/23.
 */

public class GreeterTest {
    private Greeter greeter=new Greeter();
    @Test
    public void greeterSaysHello(){
        assertThat(greeter.sayHello(),containsString("Hello"));
    }
}
