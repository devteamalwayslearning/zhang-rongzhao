package hello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by Administrator on 2017/3/30.
 */
@SpringBootApplication
public class Application {
    public static void main(String[] args)throws Exception{
        SpringApplication.run(Application.class,args);
    }
}
